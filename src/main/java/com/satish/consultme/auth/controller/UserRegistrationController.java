package com.satish.consultme.auth.controller;

import com.satish.consultme.auth.dto.UserDTO;
import com.satish.consultme.auth.service.MyUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.satish.consultme.auth.utils.constants.APIConstants.BASE_API;
import static com.satish.consultme.auth.utils.constants.APIConstants.UserRegistrationPath;

@RestController
@RequestMapping(BASE_API + UserRegistrationPath.USERS)
@RequiredArgsConstructor
public class UserRegistrationController {
    private final MyUserDetailsService userService;

    @PostMapping(UserRegistrationPath.REGISTER)
    public ResponseEntity<UserDTO> registerUser(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(this.userService.register(userDTO));
    }

    @GetMapping
    public ResponseEntity<UserDTO> getUsers() {
        return null;
    }
}

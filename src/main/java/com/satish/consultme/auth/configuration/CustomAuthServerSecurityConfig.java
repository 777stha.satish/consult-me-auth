package com.satish.consultme.auth.configuration;

import com.satish.consultme.auth.utils.constants.APIConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerSecurityConfiguration;

@Import(AuthorizationServerEndpointsConfiguration.class)
@Configuration
@Order(-1)
public class CustomAuthServerSecurityConfig extends AuthorizationServerSecurityConfiguration {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .requestMatchers()
                .antMatchers("/.well-known/jwks.json", APIConstants.UserRegistrationPath.USERS_FULL)
                .and()
                .authorizeRequests()
                .antMatchers("/.well-known/jwks.json", APIConstants.UserRegistrationPath.USERS_FULL).permitAll();
    }
}

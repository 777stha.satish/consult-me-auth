package com.satish.consultme.auth.mapper;

import com.satish.consultme.auth.dto.UserDTO;
import com.satish.consultme.auth.model.User;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper {
   UserDTO toDTO(User user);
   User toModel(UserDTO userDTO);
}

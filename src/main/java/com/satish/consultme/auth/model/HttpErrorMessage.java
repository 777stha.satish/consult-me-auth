package com.satish.consultme.auth.model;

import com.satish.consultme.auth.model.base.MessageResponse;

public class HttpErrorMessage extends MessageResponse {
    public HttpErrorMessage() {
    }

    public HttpErrorMessage(int code, String status, String message) {
        super(code, status, message);
    }
}

package com.satish.consultme.auth.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true, nullable = false)
    @EqualsAndHashCode.Include
    private String username;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    @EqualsAndHashCode.Include
    private String password;

    @Column(nullable = false)
    private boolean enabled;

    @OneToOne(cascade = CascadeType.PERSIST )
    @JoinColumn(name = "user_profile_id", nullable = false)
    UserProfile userProfile;

    public User() {}

    public User(String username, String password, boolean enabled) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
    }

    public User(User user) {
        this.username = user.username;
        this.password = user.password;
        this.enabled = user.enabled;
    }

    @PrePersist
    public void defaultValue() {
        enabled = true;
    }
}

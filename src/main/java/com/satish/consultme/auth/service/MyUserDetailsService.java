package com.satish.consultme.auth.service;

import com.satish.consultme.auth.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface MyUserDetailsService extends UserDetailsService {
    UserDTO register(UserDTO user);
}

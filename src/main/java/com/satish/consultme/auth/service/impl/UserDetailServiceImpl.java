package com.satish.consultme.auth.service.impl;

import com.satish.consultme.auth.dto.UserDTO;
import com.satish.consultme.auth.mapper.UserMapper;
import com.satish.consultme.auth.model.CustomUserDetail;
import com.satish.consultme.auth.model.User;
import com.satish.consultme.auth.repository.UserRepository;
import com.satish.consultme.auth.service.MyUserDetailsService;
import org.mapstruct.factory.Mappers;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements MyUserDetailsService {
    private final UserRepository userRepository;
    private final UserMapper userMapper = Mappers.getMapper(UserMapper.class);

    public UserDetailServiceImpl(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.findByUsernameOrEmail(username, username).orElseThrow(() -> new UsernameNotFoundException("Bad Credentials"));
        UserDetails userDetails = new CustomUserDetail(user);
        new AccountStatusUserDetailsChecker().check(userDetails);
        return userDetails;
    }

    @Override
    public UserDTO register(UserDTO user) {
        return userMapper.toDTO(userRepository.save(userMapper.toModel(user)));
    }
}

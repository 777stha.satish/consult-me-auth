package com.satish.consultme.auth.utils.constants;

public final class APIConstants {

    private APIConstants() {}

    public static final String BASE_API = "/api/v1";

    public static final class UserRegistrationPath {
        private UserRegistrationPath() {}
        public static final String USERS = "/users";
        public static final String USERS_FULL = BASE_API + USERS + "/**";
        public static final String REGISTER = "/add";
    }
}

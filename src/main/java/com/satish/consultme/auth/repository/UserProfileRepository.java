package com.satish.consultme.auth.repository;

import com.satish.consultme.auth.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
}

package com.satish.consultme.auth;

import com.satish.fieldvalidator.datajpaadapter.annotation.EnableFieldValidatorData;
import com.satish.fieldvalidator.domain.annotation.EnableFieldValidatorDom;
import com.satish.fieldvalidator.mvcadapter.annotation.EnableUI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableFieldValidatorDom
@EnableFieldValidatorData
@EnableUI
@SpringBootApplication
public class ConsultMeAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultMeAuthApplication.class);
	}

}

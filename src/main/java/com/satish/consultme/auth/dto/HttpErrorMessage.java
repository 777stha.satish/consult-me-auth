package com.satish.consultme.auth.dto;

import com.satish.consultme.auth.dto.base.BaseMessageResponse;

public class HttpErrorMessage extends BaseMessageResponse {
    public HttpErrorMessage(int code, String message) {
        super(code, message);
    }
}

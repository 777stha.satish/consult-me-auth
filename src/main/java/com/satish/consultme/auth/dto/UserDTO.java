package com.satish.consultme.auth.dto;

import com.satish.consultme.auth.dto.base.Identity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends Identity<Long> {
    private String username;
    private String password;
    private String email;
    private boolean enabled;
    private UserProfileDTO userProfile;
}

package com.satish.consultme.auth.dto;

import com.satish.consultme.auth.dto.base.Identity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class UserProfileDTO extends Identity<Long> {
    private String firstName;
    private String lastName;
}

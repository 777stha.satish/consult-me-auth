package com.satish.consultme.auth.dto.base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class Identity<T> {
    private T id;
}

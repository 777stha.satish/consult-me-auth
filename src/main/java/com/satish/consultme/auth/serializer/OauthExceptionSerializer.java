package com.satish.consultme.auth.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.satish.consultme.auth.dto.HttpErrorMessage;
import com.satish.consultme.auth.exception.CustomOAuth2Exception;

import java.io.IOException;

public class OauthExceptionSerializer extends StdSerializer<CustomOAuth2Exception> {
    public OauthExceptionSerializer() {
        super(CustomOAuth2Exception.class);
    }

    @Override
    public void serialize(CustomOAuth2Exception value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeObject(new HttpErrorMessage(value.getStatusCode(), value.getMessage()));
    }
}
